#!/usr/bin/python3
def wsl_block():
   f=open("/proc/version","r").readline().lower()
   if (("microsoft" in f) or ("wsl" in f)):
     exit(1)
   ff=open("/proc/cpuinfo","r").read()
   for line in ff.split("\n"):
      if "microcode" in line and "0xffffffff" in line:
          exit(1)